CREATE TABLE IF NOT EXISTS events
(
    id                  INTEGER PRIMARY KEY NOT NULL,
    name                TEXT                NOT NULL,
    description         TEXT                NULL,
    event_date          TEXT                NOT NULL,
    icon                TEXT                NULL,
    sound_file_path     TEXT                NULL
);

CREATE TABLE IF NOT EXISTS recurring_events
(
    id                  INTEGER PRIMARY KEY NOT NULL,
    name                TEXT                NOT NULL,
    description         TEXT                NULL,
    cron_string         TEXT                NOT NULL,
    icon                TEXT                NULL,
    sound_file_path     TEXT                NULL,
    last_notification   TEXT                NULL
);
