# ELMO - rust persistent notification

<img src="./elmo-logo.png" alt="Elmo logo" width="200" height="200">

*A little attention hungry tool as reminder for stuff that you need to do.*

Minimal implementation of a reminder service that is not limited to a single computer session.
For people that live in the terminal something simple like:
```bash
sleep 40m && notify-send -t 3000 "Apple pie" "Fetch the apple pie from the oven" &
```
Works quite nice but it has two problems from my point of view:
1. You have to calculate the minutes from now until the target time in case you want a reminder at a specific date.
2. This simply doesn't work anymore, if I have to reboot or if the target date is a few days in the future and I shut down my PC regularly.

We solve this problem by maintaining the events within a local SQLite database and check the entries of that database in a regular interval with a good old cronjob.

## Installation

```bash
cargo install elmo
```

### Database location

By default a new database file will be created at: `~/.elmo/events.db`. If you prefer another location simply state that location with the `ELMO_DATABASE_URL` environment variable. In any case, the tables will be created automatically.

## Usage

The tool comes with 4 commands:
1. **set** - Create a new notification in one of 4 flavors:
    * <u>*Notify instantly*</u>, this is the default which is used when neither `--date`, `--cron`, or `--wait` are used. You can use this similar to other tools like `notify-send` or `dunstify`.
    * <u>*Schedule a notification*</u> when you specify a date with the `--date` option (Format: *2021-09-10T18:00*) or when you specify a duration to wait with the `--wait` option (Like *5 minutes* or *7 hours*), the event is stored within a local database, where it can be searched and announced later.
    * <u>*Schedule a recurring event*</u> with the option `--cron`, which works quite similar to setting up a cronjob manually in your crontab like `* */5 * * * elmo set "Notify me" "Every 5 hours"`, but with a few advantages. You can list all your recurring events: `elmo list --cron`, the notification is sent with a delay if you turn on your computer a few minutes after the event occurred, all your recurring events are handled by a single cronjob (less clutter in your crontab)

2. **check** - Search for events that are due right now. For regular events this will also handle all events from the past, every regular event that has been notified is deleted from the database. Additionally, the function checks for matching dates to recurring events within the time span (now - 30 minutes -> now), when an event is triggered, the date of the last notification is updated in the database.

3. **list** - Show upcoming events with all connected attributes (name, description, notification date, icon name, path to the sound file). Comes with two modes *regular events* without setting `--cron` and *recurring events* when `--cron` is set. With `--exp` you can filter the results down to events that are notified within the time span of (now -> now + duration expression), examples for expressions: `5 min`, `30 minutes`, `20 seconds`, `3 hours`, `15 days`, etc.

4. **delete** - Delete an existing event on the database with an ID locatable with the *list* command. This command also comes in two modes *regular events* and *recurring events* (`--cron`). By default, the user will be asked for confirmation, which can be skipped with the `-y` parameter.

### Examples

**Simple notification, that is triggered immediately**
```bash
elmo set "Hey" "It's me, your computer"
```

**Notification triggered at a specific time with a duration expression, with an icon**  
(*This event will be stored into the database and must be triggered with the **check** command*)
```bash
elmo set "Pizza" "Get the pizza out of the oven" --wait "20 min" --icon pizza_icon
```

**Reminder to a specific date-time, with sound, announcement & sound test**  
(*This event will be stored into the database and must be triggered with the **check** command*)
```bash
elmo set "Meeting" "Work meeting per jitsi"\
    --date "2022-10-01T11:00"\
    --sound ~/Music/gong.mp3\
    --announce\
    --test_sound
```

**Remind me every 3 days at 6 pm to water the plants**  
(*This event will be stored into the database and must be triggered with the **check** command*)
```bash
elmo set "Plants" "Water the plants" --cron "0 0 18 */3 * *" --icon plants_icon
```

## Cronjob configuration

After new events have been created, we need a mechanism to check when the notification should be emitted. Unix systems come with a well-known tool called cronjob, which enables the user to define certain recurring events.  
Open your crontab with `crontab -e` and add a line at the end to run `elmo --check` in a regular rhythm. For example (every 10 minutes):
```bash
*/10 * * * * elmo --check
```

Next, we need to make sure that the process from the cronjob can communicate with the graphic sub-system (X11). Add the following line at the top of your crontab:
```bash
XDG_RUNTIME_DIR="/run/user/1000"
```

And finally, in case you want to use a custom database location, you must also provide the `ELMO_DATABASE_URL` environment variable to the crontab:
```bash
ELMO_DATABASE_URL=/path/to/my/events.db
```

### Windows systems

The Windows ecosystem doesn't really provide an equivalent to cronjobs, it provides the Windows Task Scheduler but it is not possible to schedule a run every 10 minutes. There are some alternatives like [Task Till Dawn](https://www.oliver-matuschin.de/en/projects/task-till-dawn), but because this software is not open-source I can not recommend it with a clear conscience.  
In case you discover a good method feel free to open a pull request for this README.

#### Why Elmo?

Well my dog's name is Elmo and from time to time he is wonderful at drawing attention. ;)
